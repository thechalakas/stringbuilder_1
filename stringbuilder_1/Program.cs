﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stringbuilder_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //creating a string builder object
            StringBuilder hello_loop = new StringBuilder();

            for (int i = 0; i < 100; i++)
                hello_loop = hello_loop.Append(" wow");

            Console.WriteLine(hello_loop);

            Console.ReadLine();
        }
    }
}
